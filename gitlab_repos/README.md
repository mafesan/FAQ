
## Structure under Bitergia/c/

This documentation explains the structure used in Gitlab to provide the data
needed to set up a dashboard.

Inside the project you'll see different repositories created to allow you set
different access rules depending on the content. These are the different
repos and its goal:

* /support: this is the place where we expect you and your team to add your questions or bugs related to the service provided by Bitergia
* /sources: you'll find here a JSON file with repositories monitored grouped by project
* /reports: PDF files with published quarterly reports
* /profiles: YAML file containing accounts and organization information for contributors. This is useful when you want to modify the information shown for specific contributors. Similar to what can be done with gitdm files but with more
data sources (for more info see [some documentation about the format](https://gitlab.com/Bitergia/c/FAQ/blob/master/identities/README.md))
* /organizations: YAML file with organizations and domains mapping (for more info see [some documentation about the format](https://gitlab.com/Bitergia/c/FAQ/blob/master/identities/README.md))

There is an example of setup at https://gitlab.com/Bitergia/c/GrimoireLab, this configuration is used to provide the dashboard at https://grimoirelab.biterg.io
